export const state = () => ({
  gw2Characters: {},
  gw2MainCharacter: {},
})

export const mutations = {
  // gw2 api data
  setGW2Characters(state, gw2Characters) {
    state.gw2Characters = gw2Characters
  },
  setGW2MainCharacter(state, gw2MainCharacter) {
    state.gw2MainCharacter = gw2MainCharacter
  },
}
