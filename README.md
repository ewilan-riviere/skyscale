# Skyscale

A small application to display resources of my team on Guild Wars 2 MMO.

## Build Setup

```bash
yarn
```

### Local

```bash
yarn dev
```

### Production

```bash
yarn generate
```

```nginx
server {
  listen 80;
  server_name skyscale.git-projects.xyz;
  root /home/ewilan/www/skyscale/dist;

  location / {
    try_files $uri $uri/ /index.html;
  }

  location ~* \.(js|css|png|jpg|jpeg|gif|ico|eot|svg|ttf|woff|woff2)$ {
    access_log off;
    expires max;
  }
}
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## API

- [**wiki.guildwars2.com/API:Main**](https://wiki.guildwars2.com/wiki/API:Main)
- [**wiki.guildwars2.com/API:List_of_wrappers**](https://wiki.guildwars2.com/wiki/API:List_of_wrappers)
  - [**github.com/queicherius/gw2api-client**](https://github.com/queicherius/gw2api-client)
  - [**github.com/arnapou/gw2apiclient**](https://github.com/arnapou/gw2apiclient)
- [**Official Asset Kit**](https://www.guildwars2.com/en/media/asset-kit/)
- <https://en-forum.guildwars2.com/discussion/4852/free-for-use-fonts-similar-to-recommended-by-guild-wars-2-asset-kit>
- <https://www.guildwars2roleplayers.com/forum/m/2737230/viewthread/5147207-fonts-tools-to-mimic-gw2-style>
- <https://en-forum.guildwars2.com/discussion/87279/profession-markings-for-a-group>
- <https://wiki.guildwars2.com/wiki/Profession>
- <https://www.guildwars2.com/en/media/>
- <https://fontmeme.com/guild-wars-2-font/>
- <https://www.guildwars2.com/en/media/wallpapers/>
- <https://www.guildwars2.com/en/media/concept-art/>
