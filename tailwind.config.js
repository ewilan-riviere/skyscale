module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
    ],
  },
  theme: {
    extend: {
      fontFamily: {
        'cagliostro-regular': ['Cagliostro-Regular'],
        mono: [
          'Menlo',
          'Monaco',
          'Consolas',
          'Liberation Mono',
          'Courier New',
          'monospace',
        ],
      },
      screens: {
        xl: '1500px',
        lg: '1100px',
        md: '900px',
        sm: '400px',
      },
      colors: {
        'blue-star-dark': '#002856',
        'blue-star-light': '#53BAAA',
      },
      backgroundImage: {
        header: "url('/images/bg-gw-header.png')",
        footer: "url('/images/bg-gw-footer.png')",
      },
      opacity: {},
    },
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
    scale: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
    animation: [
      'responsive',
      'motion-safe',
      'motion-reduce',
      'hover',
      'group-hover',
    ],
    translate: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/ui')],
}
