import Vue from 'vue'

// slugify text
const slugify = (text) => {
  if (text) {
    return text
      .toString()
      .toLowerCase()
      .replace(/["']/i, '-')
      .replace(/\s+/g, '-')
      .normalize('NFD')
      .replace(/[\u0300-\u036F]/g, '')
  }
  return false
}

Vue.prototype.$slugify = slugify

const shuffle = (a) => {
  let j, x, i
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1))
    x = a[i]
    a[i] = a[j]
    a[j] = x
  }
  return a
}

Vue.prototype.$shuffle = shuffle

const objectIsEmpty = (object) => {
  return Object.keys(object).length === 0 && object.constructor === Object
}

Vue.prototype.$objectIsEmpty = objectIsEmpty
